use femtovg::{
    renderer::OpenGl, Align, Baseline, Canvas, Color, FontId, ImageFlags, ImageId, Paint, Path,
};
use glutin::{
    event::{ElementState, Event, KeyboardInput, MouseButton, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
    ContextBuilder,
};

use rslnp::Parser as TextParser;
use std::{
    collections::{HashMap, HashSet},
    env::args,
    fs::read_to_string,
    time::Instant,
};

mod types;
use types::*;

struct DefaultRenderer<R: femtovg::Renderer> {
    canvas: Canvas<R>,
    fonts: HashMap<String, FontId>,
    images: HashMap<String, ImageId>,
}

impl<R: femtovg::Renderer> DefaultRenderer<R> {
    fn new(
        mut canvas: Canvas<R>,
        font_names: HashSet<String>,
        image_names: HashSet<String>,
    ) -> Option<Self> {
        let mut fonts = HashMap::new();
        for name in font_names {
            if let Ok(font) = canvas.add_font(&name[..]) {
                fonts.insert(name, font);
            }
        }

        let flags = ImageFlags::GENERATE_MIPMAPS | ImageFlags::REPEAT_X | ImageFlags::REPEAT_Y;
        let mut images = HashMap::new();
        for name in image_names {
            if let Ok(image) = canvas.load_image_file(&name[..], flags) {
                images.insert(name, image);
            }
        }
        Some(Self {
            canvas,
            fonts,
            images,
        })
    }
}

fn main() {
    let mut args = args();
    args.next();
    let filename = args.next().expect("No argument specified");

    let parser = TextParser::new()
        .indent(2)
        .with_brackets('(', ')', None)
        .with_comments('#')
        .with_strings('\'', None);

    let mut token_parser = parser
        .parse(
            read_to_string(filename)
                .expect("File does not contain valid chars")
                .chars(),
        )
        .expect("Text parsing failed");

    let mut node: Node = token_parser.parse_rest(&()).expect("Token parsing failed");

    let elements = node.collect_elements();

    let mut fonts = HashSet::new();
    let mut images = HashSet::new();
    node.collect_data(&mut fonts, &mut images);

    let window_size = glutin::dpi::PhysicalSize::new(1920, 1080);
    let event_loop = EventLoop::new();
    let window_builder = WindowBuilder::new()
        .with_inner_size(window_size)
        .with_resizable(false)
        .with_title("Telos");

    let windowed_context = ContextBuilder::new()
        .build_windowed(window_builder, &event_loop)
        .unwrap();
    let windowed_context = unsafe { windowed_context.make_current().unwrap() };

    let renderer = OpenGl::new(|s| windowed_context.get_proc_address(s) as *const _)
        .expect("Cannot create renderer");
    let mut canvas = Canvas::new(renderer).expect("Cannot create canvas");
    canvas.set_size(
        window_size.width as u32,
        window_size.height as u32,
        windowed_context.window().scale_factor() as f32,
    );

    let mut renderer =
        DefaultRenderer::new(canvas, fonts, images).expect("Failed to create default Renderer");

    let start = Instant::now();
    let mut prev_time = start;

    let mut view = Transform {
        offset: Position::default(),
        scale: 1.0,
    };

    let mut mouse = Position::default();
    let mut update_context = UpdateContext::default();

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::LoopDestroyed => return,
            Event::WindowEvent { ref event, .. } => match event {
                WindowEvent::Resized(physical_size) => {
                    windowed_context.resize(*physical_size);
                }
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            virtual_keycode: Some(keycode),
                            state: ElementState::Pressed,
                            ..
                        },
                    ..
                } => (),
                WindowEvent::CursorMoved { position, .. } => {
                    let size = windowed_context.window().inner_size();
                    mouse = Position(position.x as f32, position.y as f32)
                        - Size(size.width as f32, size.height as f32) / 2.0;
                }
                WindowEvent::MouseInput {
                    button: MouseButton::Left,
                    state,
                    ..
                } => match state {
                    ElementState::Pressed => {
                        let click = node.click(view, &mouse);
                        update_context.grab = Some(click);
                    }
                    ElementState::Released => update_context.grab = None,
                },
                WindowEvent::MouseWheel {
                    device_id: _,
                    delta,
                    ..
                } => match delta {
                    glutin::event::MouseScrollDelta::LineDelta(_, y) => {
                        view.offset += mouse * view.scale;
                        view.scale *= 2.0f32.powf(*y / (0x10 as f32));
                        view.offset -= mouse * view.scale;
                    }
                    _ => (),
                },
                _ => (),
            },
            Event::RedrawRequested(_) => {
                let dpi_factor = windowed_context.window().scale_factor();
                let size = windowed_context.window().inner_size();
                renderer
                    .canvas
                    .set_size(size.width, size.height, dpi_factor as f32);
                renderer
                    .canvas
                    .clear_rect(0, 0, size.width, size.height, Color::black());

                let mut view = view;
                view.offset += Size(size.width as f32, size.height as f32) / 2.0;
                //node.render(&mut renderer, view, &mouse, &Style::default());
                for element in &elements {
                    element.render(&mut renderer, view, &mouse, &Style::default());
                }

                renderer.canvas.save();
                renderer.canvas.reset();
                renderer.canvas.restore();

                renderer.canvas.flush();
                windowed_context.swap_buffers().unwrap();
            }
            Event::MainEventsCleared => {
                let frame_duration = prev_time.elapsed();

                use std::{thread, time};

                node.update(&mut view, &mouse, &update_context);
                let expected_time = time::Duration::from_millis(50);
                if frame_duration < expected_time {
                    thread::sleep(expected_time - frame_duration);
                }

                windowed_context.window().request_redraw();

                let fps = 1.0 / prev_time.elapsed().as_secs_f32();
                println!("{} FPS", fps);
                prev_time = Instant::now();
            }
            _ => (),
        }
    });
}

impl<R: femtovg::Renderer> Renderer for DefaultRenderer<R> {
    fn render_circle(&mut self, transform: &Transform, rad: f32) {
        let Position(x, y) = transform.offset;
        let mut path = Path::new();
        path.circle(x, y, rad * transform.scale);
        self.canvas
            .fill_path(&mut path, Paint::color(Color::black()));
        self.canvas
            .stroke_path(&mut path, Paint::color(Color::white()));
    }
    fn render_rectangle(&mut self, transform: &Transform, size: &Size) {
        let size = *size * transform.scale;
        let Size(w, h) = size;
        let Position(x, y) = transform.offset - size / 2.0;
        let mut path = Path::new();
        path.rect(x, y, w, h);
        self.canvas
            .fill_path(&mut path, Paint::color(Color::black()));
        self.canvas
            .stroke_path(&mut path, Paint::color(Color::white()));
    }
    fn render_line(&mut self, transform: &Transform, line: &Vec<Position>) {
        let mut path = Path::new();
        let mut points = line.into_iter();
        if let Some(first) = points.next() {
            let Position(x, y) = transform.apply(first);
            path.move_to(x, y);
            for point in points {
                let Position(x, y) = transform.apply(point);
                path.line_to(x, y);
            }
        }
        self.canvas
            .stroke_path(&mut path, Paint::color(Color::white()));
    }
    fn render_polygon(&mut self, transform: &Transform, line: &Vec<Position>) {
        let mut path = Path::new();
        let mut points = line.into_iter();
        if let Some(first) = points.next() {
            let Position(x, y) = transform.apply(first);
            path.move_to(x, y);
            for point in points {
                let Position(x, y) = transform.apply(point);
                path.line_to(x, y);
            }
            path.line_to(x, y);
        }
        self.canvas
            .stroke_path(&mut path, Paint::color(Color::white()));
        self.canvas
            .fill_path(&mut path, Paint::color(Color::black()));
    }
    fn render_text(&mut self, transform: &Transform, style: &Style<'_>, text: &str) {
        let Position(x, y) = transform.offset;
        let mut paint = Paint::default();
        if let Some(font) = self.fonts.get(style.font) {
            paint.set_font(&[*font]);
        }
        paint.set_font_size(transform.scale);
        paint.set_color(Color::black());
        let Alignment(hor, ver) = &style.alignment;
        let align = match hor {
            HorizontalAlignment::Left => Align::Left,
            HorizontalAlignment::Center => Align::Center,
            HorizontalAlignment::Right => Align::Right,
        };
        paint.set_text_align(align);
        let baseline = match ver {
            VerticalAlignment::Top => Baseline::Top,
            VerticalAlignment::Middle => Baseline::Middle,
            VerticalAlignment::Bottom => Baseline::Bottom,
        };
        paint.set_text_baseline(baseline);
        let _ = self.canvas.fill_text(x, y, text, paint);
        paint.set_color(Color::white());
        let _ = self.canvas.stroke_text(x, y, text, paint);
    }
    fn render_image(&mut self, transform: &Transform, image: &str) {
        if let Some(&image) = self.images.get(image) {
            if let Some(info) = self.canvas.image_info(image).ok() {
                let size = Size(info.width() as f32, info.height() as f32) * transform.scale;
                let Size(w, h) = size;
                let Position(x, y) = transform.offset - size / 2.0;
                let paint = Paint::image(image, x, y, w, h, 0.0, 1.0);
                let mut path = Path::new();
                path.rect(x, y, w, h);
                self.canvas.fill_path(&mut path, paint);
                self.canvas
                    .stroke_path(&mut path, Paint::color(Color::white()));
            }
        }
    }
}
