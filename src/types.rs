use derive_more::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign};
use std::{
    collections::HashSet,
    ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign},
};
use token_parser::{Error as ParseError, Parsable, Parser, Result as ParseResult};

#[derive(
    Copy,
    Clone,
    Add,
    Sub,
    Mul,
    Div,
    AddAssign,
    SubAssign,
    MulAssign,
    DivAssign,
    Default,
    Debug,
    Parsable,
)]
pub struct Position(pub f32, pub f32);

#[derive(Copy, Clone, Add, Sub, AddAssign, SubAssign, Default, Debug, Parsable)]
pub struct Size(pub f32, pub f32);

impl Add<Size> for Position {
    type Output = Position;
    fn add(self, other: Size) -> Self {
        Position(self.0 + other.0, self.1 + other.1)
    }
}

impl AddAssign<Size> for Position {
    fn add_assign(&mut self, other: Size) {
        self.0 += other.0;
        self.1 += other.1;
    }
}

impl Sub<Size> for Position {
    type Output = Position;
    fn sub(self, other: Size) -> Self {
        Position(self.0 - other.0, self.1 - other.1)
    }
}

impl SubAssign<Size> for Position {
    fn sub_assign(&mut self, other: Size) {
        self.0 -= other.0;
        self.1 -= other.1;
    }
}

impl Mul<f32> for Size {
    type Output = Size;
    fn mul(self, other: f32) -> Self {
        Size(self.0 * other, self.1 * other)
    }
}

impl MulAssign<f32> for Size {
    fn mul_assign(&mut self, other: f32) {
        self.0 *= other;
        self.1 *= other;
    }
}

impl Div<f32> for Size {
    type Output = Size;
    fn div(self, other: f32) -> Self {
        Size(self.0 / other, self.1 / other)
    }
}

impl DivAssign<f32> for Size {
    fn div_assign(&mut self, other: f32) {
        self.0 *= other;
        self.1 *= other;
    }
}

#[derive(Copy, Clone)]
pub struct Transform {
    pub offset: Position,
    pub scale: f32,
}

impl Default for Transform {
    fn default() -> Self {
        Self {
            offset: Position::default(),
            scale: 1.0,
        }
    }
}

impl Transform {
    pub fn apply(&self, pos: &Position) -> Position {
        *pos * self.scale + self.offset
    }

    pub fn unapply(&self, pos: &Position) -> Position {
        (*pos - self.offset) / self.scale
    }
}

impl Mul<Transform> for Transform {
    type Output = Transform;
    fn mul(self, other: Transform) -> Self {
        let offset = self.offset + other.offset * self.scale;
        let scale = self.scale * other.scale;
        Self { offset, scale }
    }
}

impl MulAssign<Transform> for Transform {
    fn mul_assign(&mut self, other: Transform) {
        self.offset += other.offset * self.scale;
        self.scale *= other.scale;
    }
}

#[derive(Copy, Clone)]
pub enum HorizontalAlignment {
    Left,
    Center,
    Right,
}
#[derive(Copy, Clone)]
pub enum VerticalAlignment {
    Top,
    Middle,
    Bottom,
}

impl Default for HorizontalAlignment {
    fn default() -> Self {
        Self::Center
    }
}

impl Default for VerticalAlignment {
    fn default() -> Self {
        Self::Middle
    }
}

impl<C> Parsable<C> for HorizontalAlignment {
    fn parse_symbol(name: String, _context: &C) -> ParseResult<Self> {
        Ok(match &name[..] {
            "left" => Self::Left,
            "center" => Self::Center,
            "right" => Self::Right,
            _ => return Err(ParseError::InvalidElement),
        })
    }
}

impl<C> Parsable<C> for VerticalAlignment {
    fn parse_symbol(name: String, _context: &C) -> ParseResult<Self> {
        Ok(match &name[..] {
            "top" => Self::Top,
            "middle" => Self::Middle,
            "bottom" => Self::Bottom,
            _ => return Err(ParseError::InvalidElement),
        })
    }
}

#[derive(Copy, Clone, Default, Parsable)]
pub struct Alignment(pub HorizontalAlignment, pub VerticalAlignment);

#[derive(Clone, Default)]
pub struct Style<'a> {
    pub font: &'a str,
    pub alignment: Alignment,
}

#[derive(Clone)]
enum Visual {
    Circle(f32),
    Rectangle(Size),
    Line(Vec<Position>),
    Polygon(Vec<Position>),
    Text(String),
    Image(String),
}

impl Visual {
    pub fn render<R: Renderer>(&self, renderer: &mut R, transform: &Transform, style: &Style<'_>) {
        use Visual::*;
        match self {
            Circle(rad) => renderer.render_circle(&transform, *rad),
            Rectangle(size) => renderer.render_rectangle(&transform, size),
            Line(points) => renderer.render_line(&transform, points),
            Polygon(points) => renderer.render_polygon(&transform, points),
            Text(content) => renderer.render_text(&transform, &style, &content[..]),
            Image(name) => renderer.render_image(&transform, &name[..]),
        }
    }
}

pub struct Node {
    transform: Transform,

    active: bool,
    visible: bool,
    drag: bool,

    font: Option<String>,
    alignment: Option<Alignment>,

    input: bool,

    visuals: Vec<Visual>,
    children: Vec<Node>,
}

pub struct Element {
    parent: usize,

    transform: Transform,

    active: bool,
    visible: bool,
    drag: bool,

    font: String,
    alignment: Alignment,

    input: bool,

    visuals: Vec<Visual>,
}

impl<C> Parsable<C> for Node {
    fn parse_list(parser: &mut Parser, context: &C) -> ParseResult<Self> {
        let mut result = Self::new();
        for parser in parser {
            let mut parser = parser?;
            let name: String = parser.parse_next(context)?;
            match &name[..] {
                "node" => result.children.push(parser.parse_rest(context)?),

                "pos" => result.transform.offset = parser.parse_rest(context)?,
                "scale" => result.transform.scale = parser.parse_next(context)?,

                "inactive" => result.active = false,
                "hide" => result.visible = false,
                "drag" => result.drag = true,

                "font" => {
                    if result.font.replace(parser.parse_next(context)?).is_some() {
                        return Err(ParseError::InvalidElement);
                    }
                }
                "align" => {
                    if result
                        .alignment
                        .replace(parser.parse_rest(context)?)
                        .is_some()
                    {
                        return Err(ParseError::InvalidElement);
                    }
                }

                "cir" => result
                    .visuals
                    .push(Visual::Circle(parser.parse_next(context)?)),
                "rect" => result
                    .visuals
                    .push(Visual::Rectangle(parser.parse_rest(context)?)),
                "line" => result
                    .visuals
                    .push(Visual::Line(parser.parse_rest(context)?)),
                "poly" => result
                    .visuals
                    .push(Visual::Polygon(parser.parse_rest(context)?)),
                "text" => result
                    .visuals
                    .push(Visual::Text(parser.parse_next(context)?)),
                "img" => result
                    .visuals
                    .push(Visual::Image(parser.parse_next(context)?)),

                "input" => result.input = parser.parse_next(context)?,

                _ => return Err(ParseError::InvalidElement),
            }
        }
        Ok(result)
    }
}

pub trait Renderer {
    fn render_circle(&mut self, transform: &Transform, rad: f32);
    fn render_rectangle(&mut self, transform: &Transform, size: &Size);
    fn render_line(&mut self, transform: &Transform, line: &Vec<Position>);
    fn render_polygon(&mut self, transform: &Transform, line: &Vec<Position>);
    fn render_text(&mut self, transform: &Transform, style: &Style<'_>, text: &str);
    fn render_image(&mut self, transform: &Transform, image: &str);
}

pub struct ClickInfo {
    offset: Position,
    index: usize,
}

enum GrabState {
    Grab(ClickInfo),
    Found,
}

#[derive(Default)]
pub struct UpdateContext {
    pub grab: Option<ClickInfo>,
}

impl Node {
    fn new() -> Self {
        Self {
            transform: Transform::default(),

            active: true,
            visible: true,
            drag: false,

            font: None,
            alignment: None,

            input: false,

            visuals: Vec::new(),
            children: Vec::new(),
        }
    }

    pub fn collect_data(&self, fonts: &mut HashSet<String>, images: &mut HashSet<String>) {
        if let Some(name) = &self.font {
            fonts.insert(name.clone());
        }
        for visual in &self.visuals {
            if let Visual::Image(name) = visual {
                images.insert(name.clone());
            }
        }
        for node in &self.children {
            node.collect_data(fonts, images);
        }
    }

    fn get_element(
        &self,
        transform: Transform,
        visible: bool,
        active: bool,
        style: &Style<'_>,
        parent: usize,
    ) -> Element {
        let mut style = style.clone();
        Element {
            parent,

            transform,

            visible,
            active,

            drag: self.drag,
            input: self.input,

            visuals: self.visuals.clone(),

            alignment: style.alignment,
            font: style.font.to_string(),
        }
    }

    fn collect_elements_to_vec(
        &self,
        elements: &mut Vec<Element>,
        mut transform: Transform,
        mut visible: bool,
        mut active: bool,
        style: &Style<'_>,
        index: &mut usize,
    ) {
        transform *= self.transform;
        visible &= self.visible;
        active &= self.visible;
        let mut style = style.clone();
        if let Some(font) = &self.font {
            style.font = &font[..];
        }
        if let Some(alignment) = self.alignment {
            style.alignment = alignment;
        }

        elements.push(self.get_element(transform, visible, active, &style, *index));

        *index += 1;

        for node in &self.children {
            node.collect_elements_to_vec(elements, transform, visible, active, &style, index);
        }
    }

    pub fn collect_elements(&self) -> Vec<Element> {
        let mut result = Vec::new();
        self.collect_elements_to_vec(
            &mut result,
            Transform::default(),
            true,
            true,
            &Style::default(),
            &mut 0,
        );
        result
    }

    pub fn render<R: Renderer>(
        &self,
        renderer: &mut R,
        mut transform: Transform,
        cursor: &Position,
        style: &Style<'_>,
    ) {
        if !self.visible {
            return;
        }
        transform *= self.transform;

        let mut style = style.clone();
        if let Some(font) = &self.font {
            style.font = &font[..];
        }
        if let Some(alignment) = self.alignment {
            style.alignment = alignment;
        }

        for visual in &self.visuals {
            visual.render(renderer, &transform, &style);
        }

        for node in &self.children {
            node.render(renderer, transform, cursor, &style);
        }
    }

    fn grab(
        &self,
        mut transform: Transform,
        cursor: &Position,
        index: &mut usize,
    ) -> Option<GrabState> {
        if !self.active {
            return None;
        }

        *index += 1;

        transform *= self.transform;

        let Position(rx, ry) = transform.unapply(cursor);
        let grab_index = *index;

        let mut result = None;

        for node in self.children.iter().rev() {
            result = node.grab(transform, cursor, index);
            if result.is_some() {
                break;
            }
        }

        if result.is_none() {
            for visual in self.visuals.iter().rev() {
                use Visual::*;
                result = match visual {
                    Circle(rad) => {
                        let rad = rad * transform.scale;
                        if rx * rx + ry * ry <= rad * rad {
                            Some(GrabState::Found)
                        } else {
                            None
                        }
                    }
                    Rectangle(size) => {
                        let Size(w, h) = *size * transform.scale;
                        if -w / 2.0 <= rx && rx <= w / 2.0 && -h / 2.0 <= ry && ry <= h / 2.0 {
                            Some(GrabState::Found)
                        } else {
                            None
                        }
                    }
                    _ => None,
                };
                if result.is_some() {
                    break;
                }
            }
        }

        if let Some(GrabState::Found) = result {
            if self.drag {
                let offset = Position(rx, ry);
                return Some(GrabState::Grab(ClickInfo {
                    index: grab_index,
                    offset,
                }));
            }
        }
        result
    }

    pub fn click(&self, transform: Transform, cursor: &Position) -> ClickInfo {
        if let Some(GrabState::Grab(click)) = self.grab(transform, cursor, &mut 0) {
            click
        } else {
            let offset = transform.unapply(cursor);
            ClickInfo { index: 0, offset }
        }
    }

    fn indexed_update(
        &mut self,
        mut transform: Transform,
        cursor: &Position,
        context: &UpdateContext,
        index: &mut usize,
    ) {
        if !self.active {
            return;
        }

        *index += 1;

        transform *= self.transform;

        if let Some(ClickInfo {
            index: grab_index,
            offset: grab_offset,
        }) = context.grab
        {
            if *index == grab_index {
                let offset = transform.unapply(cursor);
                self.transform.offset += offset - grab_offset;
                return;
            }
        }

        for node in self.children.iter_mut().rev() {
            node.indexed_update(transform, cursor, context, index);
        }
    }

    pub fn update(
        &mut self,
        transform: &mut Transform,
        cursor: &Position,
        context: &UpdateContext,
    ) {
        if let Some(ClickInfo {
            index: 0,
            offset: grab_offset,
        }) = context.grab
        {
            let offset = transform.unapply(cursor);
            transform.offset += offset - grab_offset;
        }

        self.indexed_update(*transform, cursor, context, &mut 0)
    }
}

impl Element {
    pub fn render<R: Renderer>(
        &self,
        renderer: &mut R,
        mut transform: Transform,
        cursor: &Position,
        style: &Style<'_>,
    ) {
        if !self.visible {
            return;
        }
        transform *= self.transform;

        let mut style = style.clone();
        style.font = &self.font[..];
        style.alignment = self.alignment;

        for visual in &self.visuals {
            visual.render(renderer, &transform, &style);
        }
    }

    fn grab(
        &self,
        mut transform: Transform,
        cursor: &Position,
        index: &mut usize,
    ) -> Option<GrabState> {
        if !self.active {
            return None;
        }

        *index += 1;

        transform *= self.transform;

        let Position(rx, ry) = transform.unapply(cursor);
        let grab_index = *index;

        let mut result = None;

        if result.is_none() {
            for visual in self.visuals.iter().rev() {
                use Visual::*;
                result = match visual {
                    Circle(rad) => {
                        let rad = rad * transform.scale;
                        if rx * rx + ry * ry <= rad * rad {
                            Some(GrabState::Found)
                        } else {
                            None
                        }
                    }
                    Rectangle(size) => {
                        let Size(w, h) = *size * transform.scale;
                        if -w / 2.0 <= rx && rx <= w / 2.0 && -h / 2.0 <= ry && ry <= h / 2.0 {
                            Some(GrabState::Found)
                        } else {
                            None
                        }
                    }
                    _ => None,
                };
                if result.is_some() {
                    break;
                }
            }
        }

        if let Some(GrabState::Found) = result {
            if self.drag {
                let offset = Position(rx, ry);
                return Some(GrabState::Grab(ClickInfo {
                    index: grab_index,
                    offset,
                }));
            }
        }
        result
    }

    pub fn click(&self, transform: Transform, cursor: &Position) -> ClickInfo {
        if let Some(GrabState::Grab(click)) = self.grab(transform, cursor, &mut 0) {
            click
        } else {
            let offset = transform.unapply(cursor);
            ClickInfo { index: 0, offset }
        }
    }

    fn indexed_update(
        &mut self,
        mut transform: Transform,
        cursor: &Position,
        context: &UpdateContext,
        index: &mut usize,
    ) {
        if !self.active {
            return;
        }

        *index += 1;

        transform *= self.transform;

        if let Some(ClickInfo {
            index: grab_index,
            offset: grab_offset,
        }) = context.grab
        {
            if *index == grab_index {
                let offset = transform.unapply(cursor);
                self.transform.offset += offset - grab_offset;
                return;
            }
        }
    }

    pub fn update(
        &mut self,
        transform: &mut Transform,
        cursor: &Position,
        context: &UpdateContext,
    ) {
        if let Some(ClickInfo {
            index: 0,
            offset: grab_offset,
        }) = context.grab
        {
            let offset = transform.unapply(cursor);
            transform.offset += offset - grab_offset;
        }

        self.indexed_update(*transform, cursor, context, &mut 0)
    }
}
